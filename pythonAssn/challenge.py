# Sarah Maas
# CS 344 Operating Systems, Fall 2017
# Program Py

import random

alpha = 'abcdefghijklmnopqrstuvwxyz'

#create three files, each with 10 randomly selected characters written to then
#print those characters as well
for outputFilename in [str('file' + str(x + 1) + '.txt') for x in range(3)]:
    charListString = str("".join( alpha[random.randint(0, len(alpha)-1)] for x in range(10)))
    with open(outputFilename, 'w+') as outputFile:
        outputFile.write(charListString + '\n')
        print charListString

#randomly generate two members between 1 and 42, inclusive and print them and their result
nums = [random.randint(1, 42) for x in range(2)]
print str(nums[0]) + '\n' + str(nums[1]) + '\n' + str(nums[0] * nums[1])
