# Sarah Maas
# CS 344 Operating Systems, Fall 2017
# Program Py

import random

alpha = 'abcdefghijklmnopqrstuvwxyz'

#create three files, each with 10 randomly selected characters written to then
#print those characters as well
for x in range(3):
    outputFilename = 'file' + str(x + 1) + '.txt'
    charList = []
    for num in range(10):
        charList.append(alpha[random.randint(0, len(alpha)-1)])
    with open(outputFilename, 'w+') as outputFile:
        charListString = ''
        for letter in charList: 
            outputFile.write(letter)
            charListString += letter
        outputFile.write('\n')
        print charListString

#randomly generate two members between 1 and 42, inclusive and print them and their result
num1 = random.randint(1, 42)
num2 = random.randint(1, 42)
print num1
print num2
print num1 * num2