#!/bin/bash

# Program 1
# Author: Sarah Maas
# Date: October 9, 2017
# Description: This stats program calculates the averages and medians
# for columns or rows of data, whichever is specified in the parameters
# for the script when it's called. It accepts either a filename or data
# via stdin.

function instructions() {
    (>&2 echo "./stats {-rows|-cols} [file]")
    exit 1
}

# to divide integers with proper rounding (a + (b/2)) / b
function int_divide_round() {
    #return didn't work because it wraps at 255, so using global
    RESULT=`expr $[ $1 + $[ $2 / 2 ]] / $2`
}

PID=$$
TMPFILE="temp$PID"
RESULT=0

# SIGKILL and SIGSTOP can not be caught, blocked or ignored.
trap "rm -f temp$PID*; exit 1" HUP INT TERM 

# wrong number of arguments? prompt then quit
if [[ $# < 1 ]] || [[ $# > 2 ]] ; then instructions; exit 1; fi

#handles if there's an input file or not
INPUT=${2:-/dev/stdin}

# if file name set test if file is readable
if [[ $2 ]] && [[ ! -r $2 ]] ; then >&2 echo "./stats cannot read $2"; exit 1; fi

# calculation for rows 
if [[ $1 = -r* ]] ; then printf "Average\tMedian\n"
    # for each row while line can be read
    while read LINE; do
        ROW_SUM=0
        NUM_COUNT=0
        # transpose line and sort numerically
        SORTED_LINE=$(echo $LINE | tr " " "\n" | sort -n)
        for NUM in $LINE; do
            ROW_SUM=`expr $ROW_SUM + $NUM`
            NUM_COUNT=`expr $NUM_COUNT + 1`
        done
        # calculate stats - average, midpoint index, median
        int_divide_round $ROW_SUM $NUM_COUNT
        AVERAGE=$RESULT
        # find midpoint index
        int_divide_round $NUM_COUNT 2
        MIDPOINT=$RESULT
        ODD=`expr $NUM_COUNT % 2`
        # handle even midpoint, take larger number
        if [[ ! $ODD = 1 ]]; then MIDPOINT=`expr $MIDPOINT + 1`; fi
        # find the median at the midpoint index
        COUNT=1
        for NUM in $SORTED_LINE; do
            if [[ $COUNT = $MIDPOINT ]]; then MEDIAN=$NUM; fi
            COUNT=`expr $COUNT + 1`
        done
        printf "$AVERAGE\t$MEDIAN\n"
    done <$INPUT
    
# calculation for columns    
elif [[ $1 = -c* ]]; 
    then 
    # store input so stdin doesn't get wiped 
    cat $INPUT >> $TMPFILE
    TMP_AVGS="temp$$AVG"
    TMP_MEDIANS="temp$$MEDIAN"
    # calculate the number of columns
    COLS=$(head -1 $TMPFILE | wc -w)
    # prevent errors with empty input
    if [[ $COLS = 0 ]]; then echo >> $TMP_AVGS; echo >> $TMP_MEDIANS; fi
    # iterate through the columns and store stats
    for ((i=1; i<=$COLS; i++)); do
        SORTED_LINE=$(cut -f $i $TMPFILE | sort -n)
        # get sum and number cout for the line
        COLUMN_SUM=0
        NUM_COUNT=0
        for NUM in $SORTED_LINE; do
            COLUMN_SUM=`expr $COLUMN_SUM + $NUM`
            NUM_COUNT=`expr $NUM_COUNT + 1`
        done 
        # calculate average
        int_divide_round $COLUMN_SUM $NUM_COUNT
        AVERAGE=$RESULT
        # + 1, formula works because we use the higher number with even elements
        MIDPOINT=`expr $NUM_COUNT + 1`
        int_divide_round $MIDPOINT 2
        MIDPOINT=$RESULT
        # find the median at the midpoint index
        COUNT=1
        for NUM in $SORTED_LINE; do
            if [[ $COUNT = $MIDPOINT ]]; then MEDIAN=$NUM; fi
            COUNT=`expr $COUNT + 1`
        done
        echo $AVERAGE >> $TMP_AVGS
        echo $MEDIAN >> $TMP_MEDIANS
    done
    # output stats
    printf "Averages:\n"
    cat $TMP_AVGS | tr "\n" "\t"
    echo
    printf "Medians:\n"
    cat $TMP_MEDIANS | tr "\n" "\t"
    echo

# parameter isn't r or c, prompt then quit    
else instructions; exit 1; fi               

#on exit cleanup files
rm -f temp$PID*
