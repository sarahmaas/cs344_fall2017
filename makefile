#makefile for program 2 for Operating Systems

NAME = maass

EXECS = $(NAME).adventure $(NAME).buildrooms

DIR = $(NAME).rooms

game: $(NAME).adventure.c 
	gcc -o $(NAME).adventure $(NAME).adventure.c -lpthread

rooms: $(NAME).buildrooms.c
	gcc -o $(NAME).buildrooms $(NAME).buildrooms.c  

runRooms:
	./$(NAME).buildrooms
	
runGame:
	./$(NAME).adventure

RoomsMemCheck:
	valgrind ./$(NAME).buildrooms	
	
GameMemCheck:
	valgrind ./$(NAME).adventure

catFiles:
	for D in */;							\
	do										\
	    echo "_________________________";	\
	    echo $$D;							\
	    cd $$D;								\
	    for FILE in *;						\
    do										\
	        cat $$FILE; echo;				\
	    done;								\
	    echo;								\
	    cd ..;								\
	done;
	
cleanRooms:
	rm -rf $(DIR).*

clean:
	rm -f $(EXECS) *.o *~
