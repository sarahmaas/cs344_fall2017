/*******************************************************************************
 * Author: Sarah Maas
 * Date: October 25, 2017
 * Description: This program reads in randomly generated rooms to build and 
 * run an adventure game based on the files created by buildrooms.c. The user  
 * is then able to navigate through the rooms until the end room is reached.  
 * This program utilizes threading to keep time throughout the game.
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <pthread.h>
#include <assert.h>
#include <time.h>

#define NUM_ROOMS 7
#define NUM_ROOM_NAMES 10
#define NUM_CONN_MIN 3
#define NUM_CONN_MAX 6

typedef enum {
  FALSE,
  TRUE
} boolean;

typedef enum {
    START_ROOM, 
    MID_ROOM,
    END_ROOM
} RoomType;

typedef struct {
    char* name;
    RoomType type;
    char* connections[NUM_CONN_MAX];
    int numConnect;
} Room;

pthread_mutex_t file_mutex;
pthread_mutex_t exit_mutex;

char* RoomTypeName[] = {"START_ROOM", "MID_ROOM", "END_ROOM"};
char* RoomDirPrefix = "maass.rooms.";

// function prototypes
void executeGame();
char* getNewestRoomDirectory();
Room** readRooms(char* dirName); 
void testPrint(Room** roomList);
int getRoomOfType(Room** roomList, RoomType type);
int getIndexOfRoomName(Room** roomList, char* roomName);
boolean isValidConnection(Room* currentRoom, char* userInput);
int* addToPathInt(int* path, int roomIndex, int step, int* pathCapacity);
char** addToPath(char** path, char* room, int step, int* pathCapacity);
char* addToPathString(char* path, char* room, int* pathCapacity);
void playGame(Room** roomList);
void keepTime();
void printTime();


int main() {
    // initialize mutexes
    pthread_mutex_init(&file_mutex, NULL);
    pthread_mutex_init(&exit_mutex, NULL);

    // lock exit mutex that keeps time thread running throughout the game
    pthread_mutex_lock(&exit_mutex);

    // create game threads 
    pthread_t gameThread, timeThread;
    pthread_create(&gameThread, NULL, (void*)&executeGame, NULL);
    pthread_create(&timeThread, NULL, (void*)&keepTime, NULL);

    // join threads 
    pthread_join(gameThread, NULL);
    pthread_join(timeThread, NULL);
    
    // destroy mutexs
    pthread_mutex_destroy(&file_mutex);
    pthread_mutex_destroy(&exit_mutex);
    return 0;
}


/******************************************************************************* 
 * executeGame: finds and returns most recent directory name.
 ******************************************************************************/
void executeGame() {
    Room** roomList = NULL;
    char* roomDirName = NULL;

    // get room directory and if not found, prompt and close
    roomDirName = getNewestRoomDirectory();
    if (strlen(roomDirName) == 0) {
        printf("No room directory exists, please run buildrooms. Exiting program...\n");
        free(roomDirName);
        free(roomList);
        exit(0);
    }
    
    // read in rooms and play the game
    roomList = readRooms(roomDirName);
    playGame(roomList);

    // free memory
    free(roomDirName);
    
    // free roomList Room members
    int i, j;
    for (i = 0; i < NUM_ROOMS; i++) {
        // free name, all connections, then room itself
        free(roomList[i]->name);
        for (j = 0; j < NUM_CONN_MAX; j++) {
            free(roomList[i]->connections[j]);
        }
        free(roomList[i]);
    }
    free(roomList);    
}


/******************************************************************************* 
 * getNewestRoomDirectory: finds and returns most recent directory name.
 ******************************************************************************/ 
char* getNewestRoomDirectory() {
    // based on Brewster's example readDirectory.c
    boolean dirFound = FALSE;
	int newestDirTime = -1; 
	char* newestDirName = malloc(256);
	
	// fill newestDirName with null char
	memset(newestDirName, '\0', sizeof(newestDirName));
	DIR* dirToCheck; 
	struct dirent *fileInDir; 
	struct stat dirAttributes; 
	
	// open current directory 
	dirToCheck = opendir("."); 
	// ensure the directory could be opened
	if (dirToCheck > 0) { 
		// check each entry in directory, readdir iterates through them
		while ((fileInDir = readdir(dirToCheck)) != NULL) {
			// check if prefix found in directory name
			if (strstr(fileInDir->d_name, RoomDirPrefix) != NULL) {
                // get attributes of the file (directory and check if it has 
                // the newest time
                stat(fileInDir->d_name, &dirAttributes); 
				
				if ((int)dirAttributes.st_mtime > newestDirTime) {
					newestDirTime = (int)dirAttributes.st_mtime;
					// wipe out the currently held directory, then reset
					memset(newestDirName, '\0', sizeof(newestDirName));
					strcpy(newestDirName, fileInDir->d_name);
				}
			}
		} 
	}   		
    // close directory and return the newest directory name
	closedir(dirToCheck); 
	return newestDirName;
}


/******************************************************************************* 
 * readRooms: read in rooms from given directory and returns the room list.
 ******************************************************************************/ 
Room** readRooms(char* dirName) {
    DIR* roomDir;
    struct dirent* fileInDir;
    struct stat dirAttributes;
    char* line = NULL;
    size_t len = 0;
    ssize_t read;
    
    // set up and allocate the required vars
    char fileName[500];
    memset(fileName, '\0', sizeof(fileName));
    char* tempType = malloc(sizeof(char) * 64);
    memset(tempType, '\0', sizeof(tempType));
    Room** tempRoomList = malloc(sizeof(Room) * NUM_ROOMS);
    
    // open the room directory
    roomDir = opendir(dirName);
    int roomIndex = 0;
    
    // ensure the directory could be opened
    if (roomDir > 0) {
		// check each entry in directory, readdir iterates through them
		while ((fileInDir = readdir(roomDir)) != NULL) {
		    // get attributes of the file (directory and check if it has 
            // the newest time
            stat(fileInDir->d_name, &dirAttributes); 
            
            // if it's . or .. skip to next
            if (strstr("..", fileInDir->d_name)!=NULL) {
                continue;
            }
            
            // allocate and store file name
            memset(fileName, '\0', sizeof(fileName));
            sprintf(fileName, "%s/%s", dirName, fileInDir->d_name);
            
            // open file
            FILE* file;
            file = fopen(fileName, "r");
            if (file != NULL) {
                // allocate and initialize Room in tempRoomList
                tempRoomList[roomIndex] = malloc(sizeof(Room));
                tempRoomList[roomIndex]->numConnect = 0;
                
                // allocate and set memory to null for room name
                tempRoomList[roomIndex]->name = malloc(sizeof(char) * 64);
                memset(tempRoomList[roomIndex]->name, '\0', 
                    sizeof(tempRoomList[roomIndex]->name));
                
                // allocate and set memory to null for room connections
                int i;
                for (i = 0; i < NUM_CONN_MAX; i++) {
                    tempRoomList[roomIndex]->connections[i] = malloc(sizeof(char) * 64);
                    memset(tempRoomList[roomIndex]->connections[i], '\0', 
                        sizeof(tempRoomList[roomIndex]->connections[i]));
                }

                // set pointer simply to make next block more readable
                Room* currentRoom = tempRoomList[roomIndex];
                
                // from the getline man page, iterates through each line
                while ((read = getline(&line, &len, file)) != -1) {
                    // strstr compares to identify the line type for further processing
                    if (strstr(line, "ROOM NAME: ") != NULL) {
                        // parse and set room name
                        sscanf(line, "ROOM NAME: %s", currentRoom->name);
                    } else if (strstr(line, "CONNECTION ") != NULL) {
                        // parse and set connections and connection count 
                        int connIndex = currentRoom->numConnect;
                        sscanf(line, "CONNECTION %i: %s", &currentRoom->numConnect, 
                            currentRoom->connections[connIndex]);
                    } else if (strstr(line, "ROOM TYPE: ") != NULL) {
                        // parse and set room type 
                        sscanf(line, "ROOM TYPE: %s", tempType);
                        for (i = 0; i < 3; i++) {
                            if(strcmp(tempType, RoomTypeName[i]) == 0) {
                                currentRoom->type = i;
                            }
                        }
                    }
                }
                // next room and close file
                roomIndex++;
                fclose(file); 
            }
		}
    }
    // clean up memory and close directory before returning the room list
    free(line);
    free(tempType);
    closedir(roomDir);
    return tempRoomList;
}


/******************************************************************************* 
 * getRoomOfType: get first room of given type from the given roomList.
 ******************************************************************************/ 
int getRoomOfType(Room** roomList, RoomType type) {
    int i;
    // returns first room found of the given type
    for (i = 0; i < NUM_ROOMS; i++) {
        if (roomList[i]->type == type) {
            return i;
        }
    }
    // not found, return sentinel value
    return -1;
}


/******************************************************************************* 
 * getIndexOfRoomName: get index of given room name from the given roomList.
 ******************************************************************************/ 
int getIndexOfRoomName(Room** roomList, char* roomName) {
    int i;
    // returns index of matching room name if found
    for (i = 0; i < NUM_ROOMS; i++) {
        if (strcmp(roomList[i]->name, roomName) == 0) {
            return i;
        }
    }
    // not found, return sentinel value
    return -1;
}


/******************************************************************************* 
 * isValidConnection: returns whether or not the given testString is a 
 * connection for the given Room.
 ******************************************************************************/ 
boolean isValidConnection(Room* currentRoom, char* testString) {
    int i;
    // check each connection for a name match
    for (i = 0; i < NUM_CONN_MAX; i++) {
        if (strcmp(currentRoom->connections[i], testString) == 0) {
                return TRUE;
        }
    }
    // not found, return false
    return FALSE;    
}


/******************************************************************************* 
 * printRoomMenu: returns whether or not the given testString is a 
 * connection for the given Room.
 ******************************************************************************/ 
void printRoomMenu(Room* room) {
    // print the location
    printf("CURRENT LOCATION: %s\n", room->name);
    printf("POSSIBLE CONNECTIONS: ");
    // print all connections
    int i;
    for (i = 0; i < NUM_CONN_MAX; i++) {
        char* roomName = room->connections[i];
        
        // print a comma separated list ending in a period.
        if (strlen(roomName)==0) {
            continue;
        } else if (i == 0) {
            printf("%s", roomName);
        } else {
            printf(", %s", roomName);
        }
    }
    // print the end of the list
    printf(".\n");
    printf("WHERE TO? >");
}


/******************************************************************************* 
 * addToPathString: add valid selection to the path and resize if needed.
 ******************************************************************************/ 
char* addToPathString(char* path, char* room, int* pathCapacity) {
    // test length of string, 1 for null term and 1 for newline
    int lengthNeeded = strlen(room) + 1 + 1;
    int currentlyUsed = strlen(path) + 1;
    
    // if resize is needed to fit into path, double capacity
    if (lengthNeeded + currentlyUsed > *pathCapacity) {
        *pathCapacity *= 2;
    }
    // reallocating every time because valgrind got angry when threading :)
    // concatenate name
    char* newPath = (char*) calloc(*pathCapacity, sizeof(char));
    sprintf(newPath, "%s%s\n", path, room);
    // free old path and return new
    free(path);
    return newPath;
}


/******************************************************************************* 
 * playGame: play the adventure game!
 ******************************************************************************/ 
void playGame(Room** roomList) {
    // set up vars for getline user input
    char* whereTo = NULL;
    size_t len = 0;                                                                                
    ssize_t read; 
    // starting char to be stored, will be realloc as needed
    int pathCapacity = 64;
    char* path = calloc(pathCapacity, sizeof(char));
    // set room indexes - start, end, and current to starting room   
    int startRoomIndex = getRoomOfType(roomList, START_ROOM);
    int endRoomIndex = getRoomOfType(roomList, END_ROOM);
    int currentRoomIndex = startRoomIndex;
    // step up steps and winning condition
    int stepsTaken = 0;
    boolean reachedDestination = FALSE;

    // while not at destination
    do {
        printRoomMenu(roomList[currentRoomIndex]);
        // not doing anything with read, but it will be -1 if it fails to read
        // in the line from stdin and overwrite newline char
        read = getline(&whereTo, &len, stdin);
        whereTo[strlen(whereTo) - 1] = '\0';
        //testing if the input is a room connection or time request
        boolean validConn = isValidConnection(roomList[currentRoomIndex], whereTo);
        boolean timeRequested = (strcmp(whereTo, "time") == 0);
        
        // handle user request, prompt for bad input, report time, or move to room
        if (validConn == FALSE && timeRequested == FALSE) {
            printf("\nHUH? I DON’T UNDERSTAND THAT ROOM. TRY AGAIN.\n\n");
        } else if (timeRequested == TRUE) {
            // gets time from time thread
            printTime();
        } else {
            // set selected room index and update current room for next loop
            int selectedRoom = getIndexOfRoomName(roomList, whereTo);
            currentRoomIndex = selectedRoom;
            if (currentRoomIndex == endRoomIndex) {
                reachedDestination = TRUE;
            }
            printf("\n");
            // add room to path taken, send it the pointer to the name of the room 
            // to keep from unnecessary memory allocation, and increase # of steps
            char* toName = roomList[selectedRoom]->name;
            path = addToPathString(path, toName, &pathCapacity);
            stepsTaken++;
        } 
    } while (reachedDestination == FALSE);
    
    // prints congratulations and number of steps, conditional operator appends
    // s if not 1 step
    printf("YOU HAVE FOUND THE END ROOM. CONGRATULATIONS!\n");
    printf("YOU TOOK %d STEP%s. ", stepsTaken, (stepsTaken == 1) ? "": "S" );
    printf("YOUR PATH TO VICTORY WAS:\n");
    
    // print the concatenated path
    printf("%s", path);
    
    // free all allocated memory
    free(whereTo);
    free(path);
    
    // release the time thread from its timekeeping task :)
    pthread_mutex_unlock(&exit_mutex);
}


/******************************************************************************* 
 * keepTime: runs timekeeping thread. Continuously outputs time to file.
 ******************************************************************************/ 
void keepTime() {
    // as suggested on Piazza, this mutex keeps the thread running until
    // unlocked at the end of playGame
    while(pthread_mutex_trylock(&exit_mutex) > 0) {
        // lock file mutex
        pthread_mutex_lock(&file_mutex);
        
        // get current time
        char currentTime[50];
        struct tm *timeStruct;
        time_t now = time(0);
        timeStruct = localtime(&now);
        
        // makes a concatenated string in the given format
        strftime(currentTime, sizeof(currentTime), 
            "%l:%M%P, %A, %B %e, %Y", timeStruct);
        
        // open timekeeping file
        FILE* file;
        file = fopen("currentTime.txt", "w");
        // if successfully opened, print to file
        if (file != NULL) {
            fprintf(file, "%s\n", currentTime);
            fclose(file);
        }
        
        // unlock file mutex
        pthread_mutex_unlock(&file_mutex);
        
        // wait one second to give other thread a chance, needed on os1
        sleep(1);
    } 
}


/******************************************************************************* 
 * printTime: reads in time from file and outputs to console.
 ******************************************************************************/ 
void printTime() {
    // lock file mutex
    pthread_mutex_lock(&file_mutex); 
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    
    // open file
    FILE* file;
    file = fopen("currentTime.txt", "r");
    // if file opens, get time and output to screen
    if (file != NULL) {
        getline(&line, &len, file);
        printf("\n%s\n", line);
        fclose(file);
    }
    // free memory
    free(line);
    
    // unlock file mutex  
    pthread_mutex_unlock(&file_mutex);
}
