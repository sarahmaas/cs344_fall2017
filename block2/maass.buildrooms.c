/*******************************************************************************
 * Author: Sarah Maas
 * Date: October 25, 2017
 * Description: This program randomly generates the number of defined rooms 
 * from a predefined list of rooms with the specified number of connections
 * and one of the room types. There is only one start room and one end room.
 * After the rooms are created, they are output to file in the directory 
 * specified which appends the process ID. I tried to keep it dynamic so the
 * number of rooms and connections could easily be modified/expanded if 
 * desired.
 *******************************************************************************/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

#define NUM_ROOMS 7
#define NUM_ROOM_NAMES 10
#define NUM_CONN_MIN 3
#define NUM_CONN_MAX 6

pid_t PROCESS_ID;  

typedef enum {
  FALSE,
  TRUE
} boolean;

typedef enum {
    START_ROOM, 
    MID_ROOM,
    END_ROOM
} RoomType;

typedef struct {
    char* name;
    RoomType type;
    int connections[NUM_CONN_MAX];
    int numConnect;
} Room;

char DirectoryName[] = "maass.rooms";
char DirFullName[100]; 

char* RoomTypeName[] = {"START_ROOM", "MID_ROOM", "END_ROOM"};
char* RoomNames[] = {
    "Ariel",
    "Beaumonde",
    "Haven",
    "Hera",
    "Jiangyin",
    "Miranda",
    "Osiris",
    "Persephone",
    "Triumph",
    "Whitefall"
};
Room roomList[NUM_ROOMS];
boolean RoomNameAvailable[NUM_ROOM_NAMES];

// function prototypes
void initGlobalVars();
void makeRooms();
void setRoomName(int roomIndex, int roomNameIndex);
boolean AvailableRoomName(int roomNameIndex);
boolean isGraphFull();
void AddRandomConnection();
int GetRandomRoom();
boolean CanAddConnectionFrom(int roomIndex);
boolean ConnectionAlreadyExists(int roomIndexA, int roomIndexB);
void ConnectRoom(int roomIndexA, int roomIndexB);
boolean IsSameRoom(int roomIndexA, int roomIndexB);
void generateRoomFiles();


int main() {
    
    time_t t;
    
    // Intializes random number generator 
    srand((unsigned) time(&t));
    
    // run all methods to create room files
    initGlobalVars();
    makeRooms();
    generateRoomFiles();
    return 0;
}


/******************************************************************************* 
 * initGlobalVars: This function initializes the global variables except
 * for the roomList which will be set up in its own function (makeRooms. 
 ******************************************************************************/
void initGlobalVars() {
    // process id
    PROCESS_ID = getpid(); 
    
    // room directory name
    sprintf(DirFullName, "%s.%d", DirectoryName, PROCESS_ID);
    
    // tracking for random selection without repeat 
    int i;
    for (i = 0; i < NUM_ROOM_NAMES; i++) {
        RoomNameAvailable[i] = TRUE;
    }
}


/******************************************************************************* 
 * makeRooms: This function sets up the rooms.
 ******************************************************************************/
void makeRooms() {
    int startRoom;
    int endRoom;
    int roomNameIndex;
    int i;
    
    // set up all rooms
    for (i = 0; i < NUM_ROOMS; i++) { 
        // select and set a unique random name for each room
        do {
            roomNameIndex = rand() % NUM_ROOM_NAMES;
        } while (AvailableRoomName(roomNameIndex) == FALSE);
        setRoomName(i, roomNameIndex);
        // set default values for type and starting connection
        roomList[i].type = MID_ROOM;
        roomList[i].numConnect = 0;
    }
    
    // find random, valid start and end rooms
    startRoom = rand() % NUM_ROOMS;
    do {
        endRoom = rand() % NUM_ROOMS;
    } while (startRoom == endRoom);
    
    // update room type for start and end rooms
    roomList[startRoom].type = START_ROOM;
    roomList[endRoom].type = END_ROOM;
    
    // build all room connections
    while(isGraphFull() == FALSE) {
        AddRandomConnection();
    }
}


/******************************************************************************* 
 * setRoomName: sets the room name given the room index and the room name index.
 ******************************************************************************/
void setRoomName(int roomIndex, int roomNameIndex) {
    roomList[roomIndex].name = RoomNames[roomNameIndex];
    RoomNameAvailable[roomNameIndex] = FALSE;
}


/******************************************************************************* 
 * AvailableRoomName: returns whether or not the room name at an index 
 * is available.
 ******************************************************************************/
boolean AvailableRoomName(int roomNameIndex) {
    return RoomNameAvailable[roomNameIndex];
}


/******************************************************************************* 
 * isGraphFull: determines if the number of connections is 3-6 for all rooms.
 ******************************************************************************/
boolean isGraphFull() {
    int i;
    for (i = 0; i < NUM_ROOMS; i++) {
        int conn = roomList[i].numConnect;
        // if at any point one room has less than 3 the graph isn't full
        if (conn < NUM_CONN_MIN) {
            return FALSE;
        } else if (conn > NUM_CONN_MAX) {
            printf("Error, %s has %d connections, exiting program...\n", 
                roomList[i].name, conn);
            exit(0);
        }
    }
    return TRUE;
}


/******************************************************************************* 
 * AddRandomConnection: adds a random, valid outbound connection from a Room 
 * to another Room
 ******************************************************************************/
void AddRandomConnection() {
    int roomIndexA;  
    int roomIndexB;
    boolean validRoom = FALSE;
    
    // find a valid room index A that can accept a connection
    while(validRoom == FALSE) {
        roomIndexA = GetRandomRoom();
        validRoom = CanAddConnectionFrom(roomIndexA);
    } 
    
    // find a valid room index B to connect to A
    do {
        roomIndexB = GetRandomRoom();
    } while(CanAddConnectionFrom(roomIndexB) == FALSE
            || IsSameRoom(roomIndexA, roomIndexB) == TRUE
            || ConnectionAlreadyExists(roomIndexA, roomIndexB) == TRUE);
    
    // connect the two rooms
    ConnectRoom(roomIndexA, roomIndexB);   
    ConnectRoom(roomIndexB, roomIndexA);  
}


/******************************************************************************* 
 * GetRandomRoom: returns a random Room index, does NOT validate if connection 
 * can be added.
 ******************************************************************************/
int GetRandomRoom() {
    return rand() % NUM_ROOMS;
}


/******************************************************************************* 
 * CanAddConnectionFrom: returns true if a connection can be added from Room 
 * roomIndex (< 6 outbound connections), false otherwise
 ******************************************************************************/
boolean CanAddConnectionFrom(int roomIndex) {
    return roomList[roomIndex].numConnect < NUM_CONN_MAX;
}


/******************************************************************************* 
 * ConnectionAlreadyExists: Returns true if a connection from Room roomIndexA to 
 * Room roomIndexB already exists, false otherwise
 ******************************************************************************/
boolean ConnectionAlreadyExists(int roomIndexA, int roomIndexB) {
    int connCountA = roomList[roomIndexA].numConnect;
    int a;
    for (a = 0; a < connCountA; a++) {
        // test if the connection exists between a and b
        if (roomList[roomIndexA].connections[a] == roomIndexB) {
            return TRUE;
        }
    }    
    return FALSE;
}


/******************************************************************************* 
 * ConnectRoom: onnects Rooms roomIndexA and roomIndexB together, does not 
 * check if this connection is valid.
 ******************************************************************************/
void ConnectRoom(int roomIndexA, int roomIndexB) {
    roomList[roomIndexA].connections[roomList[roomIndexA].numConnect++] = roomIndexB;
}


/******************************************************************************* 
 * IsSameRoom: tests if room indexes are equal and returns the result.
 ******************************************************************************/
boolean IsSameRoom(int roomIndexA, int roomIndexB) {
    return roomIndexA == roomIndexB;
}


/******************************************************************************* 
 * generateRoomFiles: This function makes the directory and generates room 
 * files.
 ******************************************************************************/
void generateRoomFiles() {
    int i, j;
    
    // create the room directory
    if(mkdir(DirFullName, 0777) != 0) {
        printf("There was an error creating rooms directory %s, exiting program...\n", 
                DirFullName);
        exit(0); 
    }    
    
    // create files in directory
    chdir(DirFullName);
    
    // create room files
    for (i = 0; i < NUM_ROOMS; i++) {
        FILE* file;
        file = fopen(roomList[i].name, "w");
        if (file != NULL) {
            // output the room name to the room file
            fprintf(file, "ROOM NAME: %s\n", roomList[i].name);
            
            // output all connections to the room file
            for (j = 0; j < roomList[i].numConnect; j++) {
                int connIndex = roomList[i].connections[j];
                fprintf(file, "CONNECTION %d: %s\n", j + 1, 
                    roomList[connIndex].name);
            }
            
            // output room type to the room file
            fprintf(file, "ROOM TYPE: %s\n", RoomTypeName[roomList[i].type]);
            fclose(file); 
        } else {
            printf("Error creating filename %s, exiting program...\n", roomList[i].name);
        }
    }
    // return to original directory
    chdir("..");
}

