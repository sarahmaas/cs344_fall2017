/*******************************************************************************
 * Author: Sarah Maas
 * Date: November 15, 2017
 * Description: This program is a small shell written in C. It runs command 
 * line instructions similar to other shells with some basic functionality. 
 * It allows for redirection of standard input and standard output and it will 
 * support both foreground and background processes. 
 * It supports three built in commands: exit, cd, and status. These do not
 * support input/output redirection and do not set an exit status for the 
 * status command.
 * Other commands are executed using execvp. 
 * It also supports comments, which are lines beginning with the # character.
 * A CTRL-C command from the keyboard will terminate the currently running 
 * command if any.
 * A CTRL-Z command will toggle foreground only mode with a message on which 
 * state it is currently in.
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h> 
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

#define _GNU_SOURCE
#define MAX_ARGS 512
#define MAX_USER_INPUT 2048

typedef enum {
  FALSE,
  TRUE
} boolean;

typedef struct {
    pid_t* list;
    int size;
    int capacity;
} ProcessList;

ProcessList* BackgroundProcessList;

pid_t PROCESS_ID;

int pidExitStatus = 0;
boolean ExitShell = FALSE;
char* ProcessIdString = NULL;
boolean ForegroundOnlyFlag = FALSE;
boolean BackgroundProcessFlag = FALSE;
boolean InputFlag = FALSE;
boolean OutputFlag = FALSE;


// function prototypes
void initGlobalVars();
void cleanGlobalVars();
void runShell();
char* getCommand();
char** parseCommand(char** args, char* userInput);
char* replaceString(char* string, char* search, char* replace);
void processCommand(char** args);
void exitShell();
void cd(char** args);
void status();
void executeExternalCommand(char** args);
boolean parseIO(char** args, char** inputFileName, char** outputFileName);
boolean redirectInput(char* inputFileName);
boolean redirectOutput(char* outputFileName);
void printFinishedChildren();
void setSignalHandlers();
void catchSIGINT(int signo);
void catchSIGTSTP(int signo);
ProcessList* initProcessList(int capacity);
void addToProcessList(ProcessList* processList, pid_t processID);
boolean removeFromProcessList(ProcessList* processList, pid_t processID);
void deleteProcessList(ProcessList* processList);
void printProcessList(ProcessList* processList);


int main() {
    setSignalHandlers();
    initGlobalVars();
    runShell();
    cleanGlobalVars();
    return 0;
}


/******************************************************************************* 
 * initGlobalVars: This function initializes the global variables.
 ******************************************************************************/
void initGlobalVars() {
    PROCESS_ID = getpid();
    int pLength = snprintf(NULL, 0, "%d", PROCESS_ID);
    ProcessIdString = calloc(pLength + 1, sizeof(char));
    sprintf(ProcessIdString, "%d", PROCESS_ID);
    BackgroundProcessList = initProcessList(2);
}


/******************************************************************************* 
 * cleanGlobalVars: This function deallocates the global variables as needed.
 ******************************************************************************/
void cleanGlobalVars() {
    // free allocated memory
    free(ProcessIdString);
    deleteProcessList(BackgroundProcessList);
}


/******************************************************************************* 
 * runShell: This function runs the main loop of the shell.
 ******************************************************************************/
void runShell() {
    int counter = 0;
    while(ExitShell == FALSE) {
        // extra position for null termination, will never be updated
        char** commandArgs = malloc(sizeof(char*) * (MAX_ARGS + 1));
        int i;
        for (i = 0; i < (MAX_ARGS + 1); i++) {
            commandArgs[i] = NULL;
        }
        // get command
        char* command = getCommand();
        // parse command, if any.
        commandArgs = parseCommand(commandArgs, command);
        // run command
        processCommand(commandArgs);
        // print finished background processes, if any.
        printFinishedChildren();
        // clean up memory allocation
        i = 0;
        while(commandArgs[i] != NULL) {
            free(commandArgs[i++]);
        }
        free(commandArgs);
        free(command);
    }
}


/******************************************************************************* 
 * getCommand: This function will replace a given substring with another
 * given substring and returns the new string. 
 ******************************************************************************/
char* getCommand() {
    char* userInput = NULL;
    size_t len = 0;
    ssize_t read;
    // continue to get input until it doesn't fail
    while(TRUE) {
        len = 0;
        // prompt user for command, output : to screen
        printf(": ");
        fflush(stdout); 
        // read in the command from the user
        read = getline(&userInput, &len, stdin);
        fflush(stdin);
        // handle failure of getline
        if (read == -1) {
            clearerr(stdin);
            free(userInput);
        } else {
            break;
        } 
    }
    userInput[read - 1] = '\0';
    // return command
    return userInput;
}


/******************************************************************************* 
 * parseCommand: This function is a string tokenizer that splits the command
 * entered by spaces. 
 ******************************************************************************/
char** parseCommand(char** args, char* userInput) {
    // if no user input
    if (strlen(userInput) == 0) {
        args[0] = NULL;
        return args;
    }
    int count = 0;
    // iterate through user input and parse $$
    userInput = strtok(userInput, " ");
    do {
        char* pidInput = replaceString(userInput, "$$", ProcessIdString);
        // add to args array
        size_t length = strlen(pidInput) + 1;
        args[count] = calloc(length, sizeof(char));
        memcpy(args[count++], pidInput, length);
        // clean up memory
        free(pidInput);
    } while((userInput = strtok(NULL, " ")) != NULL); 
    // set background status
    BackgroundProcessFlag = (strcmp(args[count - 1], "&")  == 0);
    // wipe &
    if (BackgroundProcessFlag == TRUE) {
        free(args[count - 1]);
        args[count - 1] = NULL;
    }
    return args;
}


/******************************************************************************* 
 * replaceString: This function will replace a given substring with another
 * given substring and returns the new string. 
 ******************************************************************************/
char* replaceString(char* string, char* search, char* replace) {
    int bufferSize = strlen(string) + strlen(replace) + 1;
    char* buffer = (char*) calloc(bufferSize, sizeof(char));
    // set up iteration pointers
    char* strPtr = string;
    char* bufPtr = buffer;
    char* lastStrPtr = string;
    // while search term is found    
    while(strPtr = strstr(strPtr, search)) {
        // check length for overflow, resize buffer
        // current length + new str piece + increase + null term.
        int reqLength = strlen(buffer) + (int)(strPtr - lastStrPtr) 
            + strlen(replace) - strlen(search) + 1;
        if (reqLength > bufferSize) {
            bufferSize += reqLength;
            char* newBuffer = calloc(bufferSize, sizeof(char));
            memcpy(newBuffer, buffer, strlen(buffer));
            // update iteration pointer to new location
            bufPtr = newBuffer + strlen(newBuffer);
            free(buffer);
            buffer = newBuffer;
        }        
        // concatenate string up through replacement
        sprintf(bufPtr, "%.*s%s", (int)(strPtr - lastStrPtr), lastStrPtr, replace);
        // update to point at end of string
        bufPtr = strlen(buffer) + buffer;
        // update to point at the remaining string after $$
        strPtr += strlen(search);
        lastStrPtr = strPtr;
    }
    // concatenate the end
    strcat(bufPtr, lastStrPtr);
    return buffer;
}


/******************************************************************************* 
 * processCommand: This function determines the type of command received and 
 * processes it accordingly by calling the appropriate function. 
 ******************************************************************************/
void processCommand(char** args) {
    if (args[0] == NULL || strncmp(args[0], "#", 1) == 0) {
        // empty or first char is # and is a comment, ignore
        return;
    } else if (strcmp(args[0], "exit") == 0) {
        // exit command, exit shell
        exitShell(); 
    } else if (strcmp(args[0], "cd") == 0) {
        // cd command, change directory
        cd(args);
    } else if (strcmp(args[0], "status") == 0) {
        // status command, print status
        status();
    } else {
        // otherwise execute the existing command with exec
        executeExternalCommand(args);
    }
}


/******************************************************************************* 
 * exitShell: This function sets up exit for the shell via the global exit 
 * status. All background child processes are killed and cleaned up.
 ******************************************************************************/
void exitShell() {
    int status;
    pid_t caught;
    ExitShell = TRUE;
    // kill all remaining background child processes
    struct sigaction ignore_action = {0};
    ignore_action.sa_handler = SIG_IGN;
    sigaction(SIGHUP, &ignore_action, NULL);
    kill(-PROCESS_ID, SIGHUP);
    // reap all the zombies <- this is why they're tracked in global array
    int i;
    for (i = 0; i < BackgroundProcessList->size; i++) {
        waitpid(BackgroundProcessList->list[i], &status, 0);
    }
}


/******************************************************************************* 
 * cd: This function changes directory based on the given arguments. Given no
 * argument it changes to the home directory.
 ******************************************************************************/
void cd(char** args) {
    // need to change into a directory, either relative or not
    char* directory = (args[1] == NULL) ? getenv("HOME"): args[1];
    // print error if failure
    if (chdir(directory) == -1) {
        perror("chdir");
        pidExitStatus = 1;
    }
}


/******************************************************************************* 
 * status: This function will print either the exit status or terminating 
 * signal of the last foreground process.
 ******************************************************************************/
void status() {
    if (WIFEXITED(pidExitStatus)) {
        printf("exit value %d\n", WEXITSTATUS(pidExitStatus));
    } else if (WIFSIGNALED(pidExitStatus)) {
        printf("terminated by signal %d\n", WTERMSIG(pidExitStatus));
    }
    fflush(stdout);
}


/******************************************************************************* 
 * executeExternalCommand: This function executes an external command in its
 * own child process, including background processes as requested. 
 ******************************************************************************/
void executeExternalCommand(char** args) {
    char* inputFileName = NULL;
    char* outputFileName = NULL;
    // i/o redirection detection, skip command if bad input 
    boolean valid = parseIO(args, &inputFileName, &outputFileName);
    if (!valid) {
        pidExitStatus = 1;
        return;
    }
    // fork
    pid_t spawnpid = -5;
    pidExitStatus = -5;
    spawnpid = fork();
    if (spawnpid == 0) {
        // I am the child! Execute things.
        // ignore SIGTSTP, changes made during process only affect next command
        struct sigaction ignore_action = {0};
        ignore_action.sa_handler = SIG_IGN;
        sigaction(SIGTSTP, &ignore_action, NULL);
        // input, output, files, oh my! set background
        if (ForegroundOnlyFlag == FALSE && BackgroundProcessFlag == TRUE) {
            // don't kill background with SIGINT
            sigaction(SIGINT, &ignore_action, NULL);
            // background process, redirect to /dev/null if not to/from file
            if (inputFileName == NULL) {
                asprintf(&inputFileName, "%s", "/dev/null");
            } else if (outputFileName == NULL) {
                asprintf(&inputFileName, "%s", "/dev/null");
            }
        }
        // open files to redirect I/O
        redirectInput(inputFileName);
        redirectOutput(outputFileName);
        // execute command
        execvp(args[0], args);
        printf("%s: command not found\n", args[0]);
        fflush(stdout);
        exit(1); 
    }
    // waits for nonbackground and prints if terminated
    pid_t caught;
    if (ForegroundOnlyFlag == TRUE || BackgroundProcessFlag == FALSE) {
        caught = waitpid(spawnpid, &pidExitStatus, 0);
        if (WIFSIGNALED(pidExitStatus)) {
            printf("terminated by signal %d\n", WTERMSIG(pidExitStatus));
            fflush(stdout);
        }
    } else {
        printf("background pid is %d\n", (int)spawnpid);
        fflush(stdout);
        addToProcessList(BackgroundProcessList, spawnpid);
    }
    free(inputFileName);
    free(outputFileName);
}


/******************************************************************************* 
 * parseIO: This function parses through the given command to set input and
 * sources as found and remove them from the command to for processing. 
 ******************************************************************************/
boolean parseIO(char** args, char** inputFileName, char** outputFileName) {
    boolean InputFlag = FALSE;
    boolean OutputFlag = FALSE;
    int i = 0;
    // iterate through all arguments to parse out I/O symbols
    while ((i + 1 < MAX_ARGS) && args[i] != NULL) {
        // check for I/O and remove as needed
        boolean argIsInput = (strcmp(args[i], "<") == 0);
        boolean argIsOutput = (strcmp(args[i], ">") == 0);
        if (argIsInput == TRUE || argIsOutput == TRUE) {
            // this is I/O, save the two operators
            char* redirectOperator = args[i];
            char* fileName = args[i + 1];
            if(fileName == NULL){
                printf("Error: No %s file provided.\n", 
                    (argIsInput == TRUE) ? "input": "output" );
                fflush(stdout);
                return FALSE;
            } else {
                // a file name has been given!! Hooray.
                // set flags and stash filename
                if(argIsInput == TRUE) {
                    // store input filename
                    InputFlag = TRUE;
                    asprintf(inputFileName, "%s", fileName);
                } else if (argIsOutput == TRUE) {
                    // store output filename
                    OutputFlag = TRUE;
                    asprintf(outputFileName, "%s", fileName);
                }
                // checks if NEXT is NULL, so it resets last one in the loop :)
                // free both the redirection and filename 
                free(redirectOperator);
                free(fileName);
                int j = i + 2;
                //shift array after removal of the I/O command
                while((j < MAX_ARGS) && args[j] != NULL){
                    args[j - 2] = args[j];
                    j++;
                }
                // set last two positions to NULL
                args[j - 1] = NULL;
                args[j - 2] = NULL;
                // shift i back one to account for the shift
                i--;
            }
        }
        i++;
    }
    // passed, GOOD INPUT!
    return TRUE;
}


/******************************************************************************* 
 * redirectInput: This function redirects input to the specified file name 
 * including handling if the input is to /dev/null.
 ******************************************************************************/
boolean redirectInput(char* inputFileName) {
    int inputFile = STDIN_FILENO;
    if(inputFileName != NULL) {
        // open input file
        inputFile = open(inputFileName, O_RDONLY);
        if(inputFile == -1) {
            // error opening the file
            perror("Error opening input file");   
            exit(1);
        }
        else {
            // redirect stdin to the input file
            dup2(inputFile, STDIN_FILENO);
            close(inputFile);
        }
    }
}


/******************************************************************************* 
 * redirectOutput: This function redirects output to the specified file name 
 * including handling if the output is to /dev/null.
 ******************************************************************************/
boolean redirectOutput(char* outputFileName) {
    int outputFile = STDOUT_FILENO;
    if(outputFileName != NULL) {
        // opens output file if exists, creates one if not
        outputFile = open(outputFileName, O_RDWR | O_CREAT, 0777);
        if(outputFile == -1) {
            // error opening the file
            perror("Error opening output file");
            pidExitStatus = 1;
        }
        if(strcmp(outputFileName, "/dev/null") != 0) {
            if(chmod(outputFileName, S_IRWXU | S_IRWXG | S_IXOTH) == -1) {
                perror("Error with chmod of output file");
                exit(1);
            }
        }
        //send output file to stdout
        dup2(outputFile, STDOUT_FILENO);
        close(outputFile);
    }  
}


/******************************************************************************* 
 * printFinishedChildren: This function reaps zombie processes and prints out
 * their results.
 ******************************************************************************/
void printFinishedChildren(){
    int status;
    pid_t process;
    while ((process = waitpid(-1, &status, WNOHANG)) > 0) {
        // print status, like this
        // background pid 4941 is done: terminated by signal 15
        printf("background pid %d is done: ", process);
        // print status, exit or termination
        if (WIFEXITED(status)) {
            printf("exit value %d\n", WEXITSTATUS(status));
        } else if (WIFSIGNALED(status)) {
            printf("terminated by signal %d\n", WTERMSIG(status));
        }
        fflush(stdout);
        // remove from list
        removeFromProcessList(BackgroundProcessList, process);
    }
}


/******************************************************************************* 
 * setSignalHandlers: This function sets up and activates signal handlers for 
 * SIGINT and SIGTSTP.
 ******************************************************************************/
void setSignalHandlers() {
    struct sigaction SIGINT_action = {0};
    struct sigaction SIGTSTP_action = {0};
    // SIGINT parameter setup
    SIGINT_action.sa_handler = catchSIGINT;
    sigfillset(&SIGINT_action.sa_mask);
    SIGINT_action.sa_flags = SA_RESTART;
    // SIGTSTP parameter setup
    SIGTSTP_action.sa_handler = catchSIGTSTP;
    sigfillset(&SIGTSTP_action.sa_mask);
    SIGTSTP_action.sa_flags = SA_RESTART;
    // activate signal handlers
    sigaction(SIGINT, &SIGINT_action, NULL);
    sigaction(SIGTSTP, &SIGTSTP_action, NULL);
}


/******************************************************************************* 
 * catchSIGINT: This function is a signal handler for SIGINT. It stops the
 * signal from killing the process and prints a newline to keep the shell 
 * cleaner for readability.
 ******************************************************************************/
void catchSIGINT(int signo) {
    write(STDOUT_FILENO, "\n", 1);
    fflush(stdout);
}


/******************************************************************************* 
 * catchSIGTSTP: This function is a signal handler for SIGTSTP. It toggles 
 * foreground process only mode on and off. All background process requests 
 * are run in the foreground when this is on.
 ******************************************************************************/
void catchSIGTSTP(int signo) {
    // toggle flag
    ForegroundOnlyFlag = (ForegroundOnlyFlag == TRUE) ? FALSE: TRUE;
    // print appropriate message
    if (ForegroundOnlyFlag == TRUE) {
        char* message = "\nEntering foreground-only mode (& is now ignored)\n";
        write(STDOUT_FILENO, message, 50);
    } else {
        char* message  = "\nExiting foreground-only mode\n";
        write(STDOUT_FILENO, message, 30);
    }
    fflush(stdout);
}


/******************************************************************************* 
 * initProcessList: This function initializes a ProcessList of the given size.
 ******************************************************************************/
ProcessList* initProcessList(int capacity) {
    ProcessList* newList = malloc(sizeof(ProcessList));
    // initialize values
    newList->list = NULL;
    newList->size = 0;
    newList->capacity = capacity;
    // allocate array
    newList->list = malloc(sizeof(pid_t) * newList->capacity);
    return newList;
}


/******************************************************************************* 
 * addToProcessList: This function adds a given process ID to the given
 * ProcessList.
 ******************************************************************************/
void addToProcessList(ProcessList* processList, pid_t processID) {
    // test if resize needed
    if (processList->size == processList->capacity) {
        // dynamically resize array
        // new list with double capacity
        int newCapacity = 2 * processList->capacity;
        pid_t* newList = malloc(sizeof(pid_t) * newCapacity);
        // copy old list to new list
        int i;
        for (i = 0; i < processList->size; i++) {
            newList[i] = processList->list[i];
        }        
        // free old list
        free(processList->list);
        // assign pointer
        processList->list = newList;
        // update capacity
        processList->capacity = newCapacity;
    }
    // add to list
    processList->list[processList->size++] = processID;
}


/******************************************************************************* 
 * removeFromProcessList: This function removes a given process ID from the 
 * given ProcessList.
 ******************************************************************************/
boolean removeFromProcessList(ProcessList* processList, pid_t processID) {
    int i, j;
    // iterate through list to find
    for (i = 0; i < processList->size; i++) {
        if (processList->list[i] == processID) {
            for (j = i; j < processList->size - 1; j++) {
                // update rest of list
                processList->list[j] = processList->list[j + 1];
            }
            processList->size--;
            return TRUE;
        }
    }
    return FALSE;
}


/******************************************************************************* 
 * deleteProcessList: This function deletes the given ProcessList.
 ******************************************************************************/
void deleteProcessList(ProcessList* processList) {
    free(processList->list);
    free(processList);
}





