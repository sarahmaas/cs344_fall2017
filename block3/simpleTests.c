// testing things
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>

#define _GNU_SOURCE

pid_t PROCESS_ID;  
int EXIT_VALUE;
void strReplaceNew(char* string, char* search, char* replace);
char* replace$$(char* string);
char* strReplace(char* string, char* search, char* replace);
char* replaceString(char* string, char* search, char* replace);
void testReplaceString() ;
void testAsprintf(char* testString, char** pleasePersist) ;
int main() {

    char* testString = calloc(30, sizeof(char));
    char* pleasePersist = NULL;
    sprintf(testString, "I am a test string.\n");  
    testAsprintf(testString, &pleasePersist);
    printf("%s\n", pleasePersist);
    free(testString);
    free(pleasePersist);
    return 0;
}

void testAsprintf(char* testString, char** pleasePersist) {
    asprintf(pleasePersist, "%s", testString);
    printf("%s\n", *pleasePersist);

}


void cd(char* directory) {
    // need to change into a directory, either relative or not
    if (strlen(directory) == 0) {
        sprintf(directory, "%s", getenv("HOME"));
    };
    if (chdir(directory) == -1) {
        perror("chdir");
        EXIT_VALUE = 1;
    };
}




void testReplaceString() {
    //char* testString = "Hello there $$I am a $$thing"; 
    char* testString = calloc(30, sizeof(char));
    sprintf(testString, "Hello there $$I am a $$thing");
    char* search = "$$"; 
    char* result = strstr(testString, search); 
    printf("%s\n", result); 
    PROCESS_ID = getpid(); 
    char* procIdString = calloc(20, sizeof(char));
    sprintf(procIdString, "%d", PROCESS_ID);
    

    int n = 3;
    n = result - testString;
    /* print at most first three characters (safe) */
    //printf("%.*s\n", n, testString);

    int reqLength = strlen(testString) + 10 + 2 + 1;

    char* newNewString = replaceString(testString, "$$", procIdString);
    //printf("replaceString: %s\n", testString);
    printf("replaceString: %s\n", newNewString);    
    free(procIdString);
    free(testString);
    free(newNewString);    
}


char* replaceString(char* string, char* search, char* replace) {
    int bufferSize = strlen(string) + strlen(replace) + 1;
    char* buffer = (char*) calloc(bufferSize, sizeof(char));
    // set up iteration pointers
    char* strPtr = string;
    char* bufPtr = buffer;
    char* lastStrPtr = string;
    // while search term is found    
    while(strPtr = strstr(strPtr, search)) {
        // check length for overflow, resize buffer
        // current length + new str piece + increase + null term.
        int reqLength = strlen(buffer) + (int)(strPtr - lastStrPtr) 
            + strlen(replace) - strlen(search) + 1;
        if (reqLength > bufferSize) {
            bufferSize += reqLength;
            char* newBuffer = calloc(bufferSize, sizeof(char));
            memcpy(newBuffer, buffer, strlen(buffer));
            // update iteration pointer to new location
            bufPtr = newBuffer + strlen(newBuffer);
            free(buffer);
            buffer = newBuffer;
        }        
        // concatenate string up through replacement
        sprintf(bufPtr, "%.*s%s", (int)(strPtr - lastStrPtr), lastStrPtr, replace);
        // update to point at end of string
        bufPtr = strlen(buffer) + buffer;
        // update to point at the remaining string after $$
        strPtr += strlen(search);
        lastStrPtr = strPtr;
    }
    // concatenate the end
    strcat(bufPtr, lastStrPtr);
    return buffer;
}