void strreplace(char string[], char search[], char replace[]){
    char buffer[100];
    memset(buffer, '\0', sizeof(buffer));
    char* p = string;
    while((p = strstr(p, search))!= NULL){
        strncpy(buffer, string, p-string);
        strcat(buffer, replace);
        strcat(buffer, p+strlen(search));
        strcpy(string, buffer);
        p++;
    }
}

char* strReplace(char* string, char* search, char* replace){
    //char buffer[100];
    //memset(buffer, '\0', sizeof(buffer));
    int bufferSize = 64;
    char* buffer = (char*) calloc(bufferSize, sizeof(char));
    char* loc = string;

    while(loc = strstr(loc, search)){
        // check length for overflow, resize buffer
        int reqLength = strlen(buffer) + strlen(replace) - strlen(search) + 1;
        if (reqLength > bufferSize) {
            bufferSize *=2;
            char* newBuffer = calloc(bufferSize, sizeof(char));
            memcpy(newBuffer, buffer, strlen(buffer));
            //sprintf(newBuffer, "%s", buffer);
            free(buffer);
            buffer = newBuffer;
        }
        strncpy(buffer, string, loc-string);
        strcat(buffer, replace);
        strcat(buffer, loc+strlen(search));
        strcpy(string, buffer);
        loc++;
    }
    
}
