brewster [10:11 AM] 
It's def set up so you can just add a piece at a time.
Start with the main loop
Then add the exit and cd built-in commands.
Then add in the fork() exec() routine. 
Then add in background commands.
Then add in input and output redirection.
Then add in the signals
Then add in the status command. :)



main loop works
cd works
exit works, children pending
fork exec works for foreground, huzzah!
background works

signals work
status needs testing
    found that terminated foreground returns incorrect status
    
    
    
Dynamic Array

has been implemented and tested for init/delete
need to add:
    track when created - done
    track when reaped - done
    reap wait at exit - done



ProcessList* initProcessList(int capacity);
void addToProcessList(ProcessList* processList, pid_t processID);
boolean removeFromProcessList(ProcessList* processList, pid_t processID);
void deleteProcessList(ProcessList* processList);
void printProcessList(ProcessList* processList);
void testProcessList();

BackgroundProcessList