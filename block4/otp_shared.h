/*******************************************************************************
 * Author: Sarah Maas
 * Date: December 1, 2017
 * Description: This set of functions is shared between OTP enc/dec and 
 * server/client.
 *******************************************************************************/

#ifndef OTP_SHARED_H_
#define OTP_SHARED_H_

#include "cipher.h"

#define HANDSHAKE_SIZE 2
#define PACKET_SIZE 256
#define BUFFER_SIZE 1000000 
#define SEPARATOR "@@"
#define TERMINATOR "$$"

typedef enum {
    CLIENT,
    SERVER
} type;

char* serverKey;
char* serverName;
char* clientName;
cipherDirection cipher;

void readFile(char** storedText, char* fileName);
void setSignalHandlers();
void catchSIGCHLD(int signo);

#endif