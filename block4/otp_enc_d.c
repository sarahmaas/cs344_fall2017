/*******************************************************************************
 * Author: Sarah Maas
 * Date: December 1, 2017
 * Description: otp_enc_d is the server for encrypting messages received from
 * the client otp_enc.
 *******************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include "otp_shared.h"
#include "cipher.h"


// function prototypes
void error(const char* msg);
void receiveMessage(char buffer[], int bufferSize, int socket);
void sendMessage(char buffer[], int socket);
boolean receiveHandshake(int socket);
void parseMessage(char buffer[], char** key, char** text);
void initGlobalVars();


int main(int argc, char* argv[]) {

	// set up for handshake	
	initGlobalVars();
	
	int listenSocketFD, establishedConnectionFD, portNumber, charsRead;
	socklen_t sizeOfClientInfo;
	char buffer[BUFFER_SIZE];
	struct sockaddr_in serverAddress, clientAddress;
    pid_t pid;

	// test for 2 inputs and that the second is a number
	if (argc != 2 || atoi(argv[1]) < 0) {
		// prints error that shows correct syntax
		fprintf(stderr, "USAGE: %s listening_port\n", argv[0]);
		exit(1);
	}

	// set up signal handler
	setSignalHandlers();
	
    // Set up the address struct for this process (the server)
    // Clear out the address struct
	memset((char *)&serverAddress, '\0', sizeof(serverAddress)); 
	// Get the port number, convert to an integer from a string
	portNumber = atoi(argv[1]); 
	// Create a network-capable socket
	serverAddress.sin_family = AF_INET;
	// Store the port number
	serverAddress.sin_port = htons(portNumber); 
	// Any address is allowed for connection to this process
	serverAddress.sin_addr.s_addr = INADDR_ANY; 

	// Set up the socket
	// Create the socket
	listenSocketFD = socket(AF_INET, SOCK_STREAM, 0); 
	if (listenSocketFD < 0) {
		error("otp_dec_d ERROR opening socket");
	}

	// Enable the socket to begin listening
	if (bind(listenSocketFD, (struct sockaddr *)&serverAddress, 
		sizeof(serverAddress)) < 0) {
		// Connect socket to port
		error("otp_dec_d ERROR on binding");
	}
	
	// Flip the socket on - it can now receive up to 5 connections
	listen(listenSocketFD, 5); 
	
	// keep parent running to accept new connections
	while(1) {
		// Accept a connection, blocking if one is not available until one connects
		sizeOfClientInfo = sizeof(clientAddress); // Get the size of the address for the client that will connect
		establishedConnectionFD = accept(listenSocketFD, 
			(struct sockaddr *)&clientAddress, &sizeOfClientInfo); // Accept
		if (establishedConnectionFD < 0) {
			error("otp_enc_d ERROR on accept");
			continue;
		}

		pid = fork();
		if (pid == 0) {
			// I am the child
			char* message = NULL;
			char* key = NULL;
			// handshake to make sure we're communicating with the right one
			boolean valid = receiveHandshake(establishedConnectionFD);
			char* handshakeResponse = NULL;
			if (valid == 0) {
				asprintf(&handshakeResponse, "%d cannot connect to %s%s",
					valid, serverName, TERMINATOR);
				sendMessage(handshakeResponse, establishedConnectionFD);
				// Close the existing socket which is connected to the client
				close(establishedConnectionFD); 
				// Close the listening socket
				close(listenSocketFD);
				exit(0);
			} 
			asprintf(&handshakeResponse, "%d successfully connected to %s%s",
				valid, serverName, TERMINATOR);
			sendMessage(handshakeResponse, establishedConnectionFD);
			// client sends key and text to server
			receiveMessage(buffer, sizeof(buffer), establishedConnectionFD);
			// server does encrypt/decrypt
			parseMessage(buffer, &key, &message);
			char* cipheredText = cipherMessage(message, key, cipher);
			// add terminator
			char* returnCipher = NULL;
			asprintf(&returnCipher, "%s%s", cipheredText, TERMINATOR);
			// server sends back text
			sendMessage(returnCipher, establishedConnectionFD);
			// client outputs what was received		
			// server does nothing for this step :) 
			// free allocated memory
			free(message);
			free(key);
			free(cipheredText);
			free(returnCipher);
			// Close the existing socket which is connected to the client
			close(establishedConnectionFD); 
			// Close the listening socket
			close(listenSocketFD); 
			exit(0);
		} else {
			close(establishedConnectionFD);
		}
	}
    return 0;
}


/******************************************************************************* 
 * error: This function reports issues.
 ******************************************************************************/
void error(const char* msg) {
	perror(msg); 
	exit(1); 
} 


/******************************************************************************* 
 * initGlobalVars: This function initializes the global variables.
 ******************************************************************************/
void initGlobalVars() {
	serverKey = "E";
	serverName = "otp_enc_d";
	clientName = "otp_enc";
	cipher = ENCRYPT;
}


/******************************************************************************* 
 * receiveHandshake: This function checks identity of the connecting client.
 ******************************************************************************/
boolean receiveHandshake(int socket) {
	char buffer[HANDSHAKE_SIZE];
	memset(buffer, '\0', sizeof(buffer));
	// read message from the socket
	recv(socket, buffer, HANDSHAKE_SIZE - 1, 0);
	// determine if connection should be successful
	if (strncmp(buffer, serverKey, 1) == 0) {
		return TRUE;
	} else {
		return FALSE;
	}
}


/******************************************************************************* 
 * receiveMessage: This function receives a message through the given socket.
 ******************************************************************************/
void receiveMessage(char buffer[], int bufferSize, int socket) {
	// Clear out the buffer again for reuse
	memset(buffer, '\0', bufferSize);
	char packet[PACKET_SIZE];
	int totalCharsRead = 0;
	// receive packets until TERMINATOR is reached
	do {
		memset(packet, '\0', sizeof(packet)); 
		// Read data from the socket, leaving \0 at end
		int charsRead = recv(socket, packet, sizeof(packet) - 1, 0); 
		if (charsRead < 0) {
			error("SERVER: ERROR reading from socket");
			return;
		}
		// add packet to string
		strcat(buffer, packet);
		totalCharsRead += charsRead;
	} while (!strstr(buffer, TERMINATOR));
}


/******************************************************************************* 
 * sendMessage: This function sends the given message through the given socket.
 ******************************************************************************/
void sendMessage(char buffer[], int socket) {
	char packet[PACKET_SIZE];
	// Write packets until string length is reached
	int totalCharsWritten = 0;
	while (totalCharsWritten < strlen(buffer)) {
		memset(packet, '\0', sizeof(packet));
		memcpy(packet, buffer + totalCharsWritten, sizeof(packet) - 1);
		// Write data to the socket, leaving \0 at end
		int charsWritten = send(socket, packet, sizeof(packet) - 1, 0);
		if (charsWritten < 0) {
			error("SERVER: ERROR writing to socket");
		}
		// test if all of the packet was written
		if (charsWritten < strlen(packet)) {
			fprintf(stderr, "SERVER: WARNING: Not all data written to socket!\n");
			fflush(stdout);
		}
		totalCharsWritten += charsWritten;	
	}
}


/******************************************************************************* 
 * parseMessage: This function parses the given message into the key and text.
 ******************************************************************************/
void parseMessage(char buffer[], char** key, char** text) {
	// find pointers to separate string
	char* sepPtr = strstr(buffer, SEPARATOR);
	char* termPtr = strstr(buffer, TERMINATOR);
	char* startText = sepPtr + 2;
	// store key and text
	asprintf(key, "%.*s", (int)(sepPtr-buffer), buffer);
	asprintf(text, "%.*s", (int)(termPtr-startText), startText);
}



