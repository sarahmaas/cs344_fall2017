/*******************************************************************************
 * Author: Sarah Maas
 * Date: December 1, 2017
 * Description: This set of functions is shared between OTP enc/dec and 
 * server/client.
 *******************************************************************************/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h> 
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "otp_shared.h"


#define _GNU_SOURCE


/******************************************************************************* 
 * readFile: This function stores text from file in memory.
 ******************************************************************************/
void readFile(char** storedText, char* fileName) {
	int length;
	char* tmp = (char*)storedText;
	// open file
	FILE* fp = fopen(fileName, "rb");
	if(fp == NULL){
	    error("otp_enc ERROR: cannot find file");
	} else {
		// get file length
		fseek(fp, 0, SEEK_END);
		length = ftell(fp);
		fseek(fp, 0, SEEK_SET);
		// malloc len + 1 and copy file contents into string
		*storedText = calloc(length + 1, sizeof(char));
		fread(*storedText, length, 1, fp);
		fclose(fp);
		// remove newline from EOF
		if((*storedText)[length - 1] == '\n') {
			(*storedText)[length - 1] = '\0';
		}
	}
}


/******************************************************************************* 
 * setSignalHandlers: This function sets up and activates signal handlers for 
 * SIGINT and SIGTSTP.
 ******************************************************************************/
void setSignalHandlers() {
    struct sigaction SIGCHLD_action = {0};
    // SIGCHLD parameter setup
    SIGCHLD_action.sa_handler = catchSIGCHLD;
    sigfillset(&SIGCHLD_action.sa_mask);
    SIGCHLD_action.sa_flags = SA_RESTART;
    // activate signal handlers
    sigaction(SIGCHLD, &SIGCHLD_action, NULL);
}


/******************************************************************************* 
 * catchSIGCHLD: This function is a signal handler for SIGCHLD. It reaps the
 * zombie processes.
 ******************************************************************************/
void catchSIGCHLD(int signo) {
	while(waitpid(-1, NULL, WNOHANG) > 0);
}