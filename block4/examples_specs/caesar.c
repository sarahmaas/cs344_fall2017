/**
 * caesar.c
 *
 * Sarah Maas
 * sarah.ann.maas@gmail.com
 *
 * Uses numeric key provided by the user to encode a given phrase
 * by shifting it as many letters as the key specifies.
 */
 
#include <stdio.h>
#include <cs50.h>
#include <ctype.h>
#include <string.h>

int main(int argc, string argv[])
{
	// test for 2 inputs and that the second is a number
	if (argc != 2 || atoi(argv[1]) < 0)
	{
		// prints error that shows correct syntax
		printf("Usage: /caesar <key> \n");
		return 1;
	}

	// set up the key from the argument string
	int key = atoi(argv[1]);
	// reduce key for efficiency
	key %= 26;

	// get phrase from user to encode
	string phrase = GetString();

	int phraseLength = strlen(phrase);
	char currLetter;

	for (int i = 0; i < phraseLength; i++)
	{
		currLetter = phrase[i];

		// encodes letters
		if (isalpha(currLetter))
		{
			// set case and wrap, ternary operator found in CS50 Study
			bool caseUpper = isupper(currLetter);
			while ((currLetter + key) > ((caseUpper) ? 'Z' : 'z'))
			{
				currLetter -= 26;
			}
			currLetter += key;
		}
		// prints encoded character (or original for nonalpha)
		printf("%c", currLetter);
	}
	printf("\n");
	return 0;
}