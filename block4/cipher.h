/*******************************************************************************
 * Author: Sarah Maas
 * Date: December 1, 2017
 * Description: These functions will cipher a message given a key and also 
 * validate if a message is a valid cipher.
 *******************************************************************************/


#ifndef CIPHER_H_
#define CIPHER_H_

#include "boolean.h"

#define CODE_LENGTH 27

typedef enum {
  ENCRYPT,
  DECRYPT
} cipherDirection;

// extern allows the global in cipher.c to be used by other files
extern char validChars[CODE_LENGTH];

char* cipherMessage(char* message, char* key, cipherDirection cipher);
boolean validateMessage(char* message);

#endif