/*******************************************************************************
 * Author: Sarah Maas
 * Date: December 1, 2017
 * Description: These functions will cipher a message given a key and also 
 * validate if a message is a valid cipher.
 *******************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cipher.h"


// define shared global ONCE
char validChars[CODE_LENGTH] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";


// helper function prototypes
int getPosition(char c);
int cipheredPosition(char letter, char key, cipherDirection cipher);


/******************************************************************************* 
 * validateMessage: validates that a message only uses letters in the valid 
 * char array.
 ******************************************************************************/ 
boolean validateMessage(char* message) {
    // does this only contain valid chars?
    int i;
    int length = strlen(message);
    for (i = 0; i < length; i++) {
        // -1 means not found
        if (getPosition(message[i]) == -1) {
            return FALSE;
        }
    }
    return TRUE;
}


/******************************************************************************* 
 * getPosition: get position of the character in the valid char array.
 ******************************************************************************/ 
int getPosition(char c) {
    int i;
    // look for character
    for(i = 0; i < CODE_LENGTH; i++) {
        if (validChars[i] == c) {
            return i;
        }
    }
    // not found
    return -1;
}


/******************************************************************************* 
 * cipheredPosition: get ciphered character position in the valid char array.
 ******************************************************************************/ 
int cipheredPosition(char letter, char key, cipherDirection cipher) {
    switch (cipher) {
        // encrypted can just be shifted to the right
        case ENCRYPT:
            return (getPosition(letter) + getPosition(key)) % CODE_LENGTH;
        // decrypted shifts left, so mod can be negative
        case DECRYPT:
            return (getPosition(letter) - getPosition(key) + CODE_LENGTH) 
                % CODE_LENGTH;
    }
}


/******************************************************************************* 
 * cipherMessage: returns entire ciphered message, given the message, key, 
 * and cipher direction.
 ******************************************************************************/ 
char* cipherMessage(char* message, char* key, cipherDirection cipher) {
    int keyLength = strlen(key);
    int messageLength = strlen(message);
    char* ciphered = calloc(messageLength + 1, sizeof(char));
    int i;
    for(i = 0; i < messageLength; i++) {
        // determine which key char to use and get ciphered char position
        int keyPosition = i % keyLength;
        int cipherPosition = cipheredPosition(message[i], key[keyPosition], cipher);
        // store ciphered char
        ciphered[i] = validChars[cipherPosition];
    }
    return ciphered;
}

