/*******************************************************************************
 * Author: Sarah Maas
 * Date: December 1, 2017
 * Description: otp_dec is the client that connects to the server otp_dec_d to 
 * request decryption of a given message using a given key.
 *******************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include "otp_shared.h"


#define _GNU_SOURCE


char* fileText = NULL;
char* keyText = NULL;


// function prototypes
void error(const char* msg);
void connectToServer(char* port);
void initGlobalVars();
void sendHandshake(int socket);
void sendMessage(char buffer[], int socket);
void receiveMessage(char buffer[], int bufferSize, int socket);
void sendKeyText(char* key, char* file, int socket);


int main(int argc, char* argv[]) {
	// test for 4 inputs and that the fourth is a number
	if (argc != 4 || atoi(argv[3]) < 0) {
		// prints error that shows correct syntax
		fprintf(stderr, "USAGE: %s plaintext key port\n", argv[0]);
		exit(0);
	}
	// set up for handshake	
	initGlobalVars();
	// store file text and key text
	readFile(&fileText, argv[1]);
	readFile(&keyText, argv[2]);
	// otp_dec receives key or plaintext files with bad characters 
	// in them, or the key file is shorter than the plaintext, it 
	// should exit with an error, and set the exit value to 1. 
	if (strlen(keyText) < strlen(fileText)) {
		fprintf(stderr, "Error: key '%s' is too short\n", argv[2]);
		exit(1);
	}
	if (!validateMessage(fileText) || !validateMessage(keyText)) {
		fprintf(stderr, "%s error: input contains bad characters\n", serverName);
		exit(1);
	}
	// added this to prevent errors if exceeded
	if ((strlen(keyText) + strlen(fileText) + 4) > BUFFER_SIZE) {
		fprintf(stderr, "Too many chars.\n");
		exit(1);
	}
	// start connection to server
	connectToServer(argv[3]);
	// clean up memory
	free(fileText);
	free(keyText);
    return 0;
}


/******************************************************************************* 
 * error: This function reports issues.
 ******************************************************************************/
void error(const char* msg) {
	perror(msg); 
	exit(0); 
} 


/******************************************************************************* 
 * connectToServer: This function connects to the server.
 ******************************************************************************/
void connectToServer(char* port) {
	int socketFD, portNumber, charsWritten, charsRead;
	struct sockaddr_in serverAddress;
	struct hostent* serverHostInfo;
	char buffer[BUFFER_SIZE];

	// Set up the server address struct
	// Clear out the address struct
	memset((char*)&serverAddress, '\0', sizeof(serverAddress)); 
	// Get the port number, convert to an integer from a string
	portNumber = atoi(port); 
	 // Create a network-capable socket
	serverAddress.sin_family = AF_INET;
	// Store the port number
	serverAddress.sin_port = htons(portNumber); 
	// Convert the machine name into a special form of address
	serverHostInfo = gethostbyname("localhost"); 
	if (serverHostInfo == NULL) { 
		fprintf(stderr, "CLIENT: ERROR, no such host\n"); 
		exit(0); 
	}
	// Copy in the address
	memcpy((char*)&serverAddress.sin_addr.s_addr, 
		(char*)serverHostInfo->h_addr, serverHostInfo->h_length); 

	// Set up the socket
	// Create the socket
	socketFD = socket(AF_INET, SOCK_STREAM, 0); 
	if (socketFD < 0) {
		error("CLIENT: ERROR opening socket");
	}
	
	// Connect to server
	if (connect(socketFD, (struct sockaddr*)&serverAddress, 
		sizeof(serverAddress)) < 0) {
		// Connect socket to address
		fprintf(stderr, "CLIENT: ERROR connecting to port %s\n", port);
		exit(2);
	}

	// handshake to make sure we're communicating with the right one
	sendHandshake(socketFD);
	
	// receive handshake response
	receiveMessage(buffer, sizeof(buffer), socketFD);
	// handle handshake response
	if (strncmp(buffer, "0", 1) == 0) {
		char* termPtr = strstr(buffer, TERMINATOR);
		fprintf(stderr, "CLIENT: ERROR %s %.*s\n", clientName, 
			(int)(termPtr - buffer - 2), buffer + 2);
	} else {
	
		// client sends key and text to server
		sendKeyText(keyText, fileText, socketFD);
		
		// server does encrypt/decrypt
		// client does nothing for this step :)
		
		// server sends back text
		receiveMessage(buffer, sizeof(buffer), socketFD);
		// client outputs what was received
		*strstr(buffer, TERMINATOR) = '\0';
		printf("%s\n", buffer);
	}
	// Close the socket
	close(socketFD); 
}


/******************************************************************************* 
 * initGlobalVars: This function initializes the global variables.
 ******************************************************************************/
void initGlobalVars() {
	serverKey = "D";
	serverName = "otp_dec_d";
	clientName = "otp_dec";
}


/******************************************************************************* 
 * sendHandshake: This function sends identity of the client.
 ******************************************************************************/
void sendHandshake(int socket) {
	// set up handshake to send
	char buffer[HANDSHAKE_SIZE];
	memset(buffer, '\0', sizeof(buffer));
	strncpy(buffer, serverKey, HANDSHAKE_SIZE);
	// Send success back
	int charsRead = send(socket, buffer, strlen(buffer), 0); 
	if (charsRead < 0) {
		error("SERVER: ERROR writing to socket");
	}
}


/******************************************************************************* 
 * sendMessage: This function sends the given message through the given socket.
 ******************************************************************************/
void sendMessage(char buffer[], int socket) {
	char packet[PACKET_SIZE];
	// Write packets until string length is reached
	int totalCharsWritten = 0;
	while (totalCharsWritten < strlen(buffer)) {
		memset(packet, '\0', sizeof(packet));
		memcpy(packet, buffer + totalCharsWritten, sizeof(packet) - 1);
		// Write data to the socket, leaving \0 at end
		int charsWritten = send(socket, packet, sizeof(packet) - 1, 0);
		if (charsWritten < 0) {
			error("CLIENT: ERROR writing to socket");
		}
		// test if all of the packet was written
		if (charsWritten < strlen(packet)) {
			fprintf(stderr, "CLIENT: WARNING: Not all data written to socket!\n");
			fflush(stdout);
		}
		totalCharsWritten += charsWritten;	
	}
}


/******************************************************************************* 
 * receiveMessage: This function receives a message through the given socket.
 ******************************************************************************/
void receiveMessage(char buffer[], int bufferSize, int socket) {
	// Clear out the buffer again for reuse
	memset(buffer, '\0', bufferSize); 
	char packet[PACKET_SIZE];
	// receive packets until TERMINATOR is reached
	int totalCharsRead = 0;
	do {
		memset(packet, '\0', sizeof(packet)); 
		// Read data from the socket, leaving \0 at end
		int charsRead = recv(socket, packet, sizeof(packet) - 1, 0); 
		if (charsRead < 0) {
			error("CLIENT: ERROR reading from socket");
		}
		// add packet to string
		strcat(buffer, packet);
		totalCharsRead += charsRead;
	} while (!strstr(buffer, TERMINATOR));
}


/******************************************************************************* 
 * sendKeyText: This function sends the given message through the given socket.
 ******************************************************************************/
void sendKeyText(char* key, char* file, int socket) {
	// concatenate message
	char* buffer = NULL;
	asprintf(&buffer, "%s%s%s%s", key, SEPARATOR, file, TERMINATOR);
	// send to server
	sendMessage(buffer, socket);
}