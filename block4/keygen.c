/*******************************************************************************
 * Author: Sarah Maas
 * Date: December 1, 2017
 * Description: This program generates a key of specified length. The characters
 * used are in cipher.c.
 *******************************************************************************/
 

#include <stdio.h>
#include <stdlib.h>
#include "cipher.h"


int main(int argc, char* argv[]) {
	// test for 2 inputs and that the second is a number
	if (argc != 2 || atoi(argv[1]) < 0) {
		// prints error that shows correct syntax
		fprintf(stderr, "USAGE: %s keylength\n", argv[0]);
		return 1;
	}
	time_t t;
    // Intializes random number generator 
    srand((unsigned) time(&t));
    int i;
    // up to key length provided, print random char
    for (i = 0; i < atoi(argv[1]); i++) {
        // output from the validChars in cipher.h
        printf("%c", validChars[rand() % CODE_LENGTH]);
    }
    // add newline to output
    printf("\n");
    return 0;
}
