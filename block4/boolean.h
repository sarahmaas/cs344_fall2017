/*******************************************************************************
 * Author: Sarah Maas
 * Date: December 1, 2017
 * Description: This header exists simply to avoid duplicate declarations of
 * boolean.
 *******************************************************************************/

#ifndef BOOLEAN_H_
#define BOOLEAN_H_

typedef enum {
    FALSE,
    TRUE
} boolean;

#endif